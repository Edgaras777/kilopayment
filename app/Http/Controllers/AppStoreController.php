<?php

namespace App\Http\Controllers;

use App\Http\Requests\AppleStoreRequest;
use App\Service\PSPService;
use App\Service\SubscriptionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AppStoreController extends Controller
{
    const NOT_TYPE_CANCEL = 'CANCEL';
    const NOT_TYPE_INITIAL_BUY = 'INITIAL_BUY';
    const NOT_TYPE_RENEWAL = 'RENEWAL';
    const NOT_TYPE_FAIL_RENEWAL = 'DID_FAIL_TO_RENEW';

    /**
     * Request data ready to transform
     *
     * @var $data
     */
    protected $data;

    /**
     * @var SubscriptionService $subscriptionService
     */
    protected $subscriptionService;

    /**
     * @var PSPService $PspService
     */
    protected $PspService;

    /**
     * AppStoreController constructor, load needed services
     *
     * @param SubscriptionService $subscriptionService
     * @param PSPService          $PSPService
     */
    public function __construct(SubscriptionService $subscriptionService, PSPService $PSPService)
    {
        $this->subscriptionService = $subscriptionService;
        $this->PspService = $PSPService;
    }

    /**
     * Check request validation
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function checkRequest(AppleStoreRequest $request)
    {
        // TODO: Implement authentication
        return $this->transform($request);

    }

    /**
     * Select type of method
     *
     * @param Request $data
     * @return JsonResponse
     */
    public function transform(Request $data)
    {
        $methodType = $data->get('notification_type');
        $provider  = $data->get('provider');

        $transformedData = $this->PspService->transformDataFromProvider($provider, $data);

        switch ($methodType) {
            case self::NOT_TYPE_CANCEL:
                $response = $this->subscriptionService->cancelSubscription($transformedData);
                break;
            case self::NOT_TYPE_INITIAL_BUY:
                $response = $this->subscriptionService->initialSubscription($transformedData);
                break;
            case self::NOT_TYPE_FAIL_RENEWAL:
                $response = $this->subscriptionService->unsuccessfulRenewal($transformedData);
                break;
            case self::NOT_TYPE_RENEWAL:
                $response = $this->subscriptionService->renewedSubscription($transformedData);
                break;
        }

        return $response;
    }
}
