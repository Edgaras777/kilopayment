<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppleStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'auto_renew_adam_id' => 'required|min:10',
            'auto_renew_product_id' => 'required|min:10',
            'notification_type' => 'required',
        ];
    }
}
