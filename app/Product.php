<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Product extends Model
{
    use Notifiable;

    protected $table = 'products';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'user_id', 'status', 'token', 'time_to_expire'
    ];

    /**
     * Get the User that owns the product.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
