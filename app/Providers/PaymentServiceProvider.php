<?php

namespace App\Providers;

use App\Service\PSPService;
use Illuminate\Support\ServiceProvider;

class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('App/Service/PSPService', function($app) {
            return new PSPService();
        });
    }
}
