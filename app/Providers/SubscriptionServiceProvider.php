<?php

namespace App\Providers;

use App\Service\SubscriptionService;
use Illuminate\Support\ServiceProvider;

class SubscriptionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
       $this->app->bind('App/Service/SubscriptionService', function($app) {
           return new SubscriptionService();
       });
    }
}
