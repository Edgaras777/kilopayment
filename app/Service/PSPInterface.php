<?php
namespace App\Service;

interface PSPInterface{

    /**
     * Transform data from Payment provider to bussines logic data
     *
     * @param $provider
     * @param $data
     *
     * @return mixed
     */
    public function transformDataFromProvider($provider, $data);
}


