<?php

namespace App\Service;

use Carbon\Carbon;

class PSPService implements PSPInterface
{
    const PROVIDER_APPLE = "ApplePay";
    const PROVIDER_BRAINTREE = "Braintree";
    const PROVIDER_STRIPE = "Stripe";
    const PROVIDER_PAYPAL = "Paypal";

    /**
     * Transform data by payment provider
     *
     * @param $provider
     * @param $data
     * @return array|void
     */
    public function transformDataFromProvider($provider, $data)
    {
        switch ($provider) {
            case self::PROVIDER_APPLE:
                $transformedData = $this->appleDataTransform($data);
                break;
            case self::PROVIDER_BRAINTREE:
                $transformedData = $this->braintreeDataTransform($data);
                break;
            case self::PROVIDER_STRIPE:
                $transformedData  = $this->stripeDataTransform($data);
                break;
            case self::PROVIDER_PAYPAL:
                $transformedData = $this->paypalDataTransform($data);
                break;
        }

        return  $transformedData;
    }

    /**
     * Return transformated data from ApplePay
     *
     * @param $data
     * @return array
     */
    private function appleDataTransform($data)
    {
        return [
            'last_status' => $data->get('notification_type'),
            'user_id' => $data->get('auto_renew_adam_id'),
            'product_id' => $data->get('auto_renew_product_id'),
            'time_to_expire' => Carbon::now()->addDays(10),
            'expires_date' => $data->get('expires_date') ?
                Carbon::now()->addSeconds($data->get('expires_date') / 1000) : Carbon::now(),
            'is_billing_retry_period' => $data->get('is_in_billing_retry_period'),
            'cancellation_date' => $data->get('cancellation_date') ?
                Carbon::createFromFormat('Y-m-d H:m:s', $data->get('cancellation_date')) : Carbon::now(),
            'token' => $data->get('latest_receipt'),
        ];
    }

    private function braintreeDataTransform($data)
    {
        // TODO: implement Braintree data transform
    }

    private function stripeDataTransform($data)
    {
        // TODO: implement Stripe data transform
    }

    private function paypalDataTransform($data)
    {
        // TODO: implement Paypal data transform
    }
}
