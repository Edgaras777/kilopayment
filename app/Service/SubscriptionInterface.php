<?php
namespace App\Service;

interface SubscriptionInterface{

    /**
     * Create subscription of product for user
     *
     * @param $transformedData
     *
     * @return mixed
     */
    public function initialSubscription($transformedData);

    /**
     * Renew subscription
     *
     * @param $transformedData
     *
     * @return mixed
     */
    public function renewedSubscription($transformedData);

    /**
     * Handle unsuccessful renewal
     *
     * @param $transformedData
     *
     * @return mixed
     */
    public function unsuccessfulRenewal($transformedData);

    /**
     * Cancel subscription
     *
     * @param $transformedData
     *
     * @return mixed
     */
    public function cancelSubscription($transformedData);

}


