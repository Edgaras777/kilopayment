<?php

namespace App\Service;

use App\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use phpDocumentor\Reflection\DocBlock\Tags\Uses;

class SubscriptionService implements SubscriptionInterface
{

    /**
     * @param $transformedData
     *
     * @return JsonResponse
     */
    public function initialSubscription($transformedData)
    {
        // TODO: implement more logic, status and errors
        $product = $this->getProductWithUserId($transformedData);

       if (empty($product)) {
           $product = new Product();
           $product->fill($transformedData);
           $product->save();
       } else {
           $product->last_status = $transformedData['last_status'];
           $product->time_to_expire = $transformedData['time_to_expire'];
           $product->save();
       }

       return response()->json(['status' => 'Initial Subscription success'], 200);
    }

    /**
     * @param $transformedData
     *
     * @return JsonResponse
     */
    public function renewedSubscription($transformedData)
    {
        // TODO: implement check for statuses and errors
        $product = $this->getProductWithUserId($transformedData);
        $product->last_status = $transformedData['last_status'];
        $product->time_to_expire = $transformedData['expires_date'];
        $product->save();

        return response()->json(['status' => 'Renewal Success',
            'data' => 'New renewal will charge at:' . $product->time_to_expire], 200);
    }

    /**
     * @param $transformedData
     *
     * @return JsonResponse
     */
    public function unsuccessfulRenewal($transformedData)
    {
        // TODO: Implement status check and log dates, errors
        $product = $this->getProductWithUserId($transformedData);
        $product->last_status = $transformedData['last_status'];
        $product->time_to_expire = Carbon::now();
        $product->save();

        return response()->json(['status' => 'Failed to renew subscription'], 200);
    }

    /**
     * @param $transformedData
     *
     * @return JsonResponse
     */
    public function cancelSubscription($transformedData)
    {
        // TODO: Implement cancelSubscription() method.
        $product = $this->getProductWithUserId($transformedData);
        $product->last_status = $transformedData['last_status'];
        $product->time_to_expire = $transformedData['cancellation_date'];
        $product->save();

        return response()->json(['status' => 'Subscription canceled'], 200);
    }

    /**
     * @param $transformedData
     *
     * @return array
     */
    private function getProductWithUserId($transformedData)
    {
        // TODO: implement creating new user, handle errors
        $product = Product::where('product_id', $transformedData['product_id'])->get()->first();
        $user = User::where('app_id', $transformedData['user_id'])->get()->first();
        if (empty($user)) {
            // TODO: new user registration
            $user = New User();
        }
        $transformedData['user_id'] = $user->id;

        return $product;
    }

}
